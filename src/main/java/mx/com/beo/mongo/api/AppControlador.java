package mx.com.beo.mongo.api;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.mongodb.WriteResult;

import mx.com.beo.mongo.services.MongoService;
import mx.com.beo.mongo.util.Operaciones;


/**
* Copyright (c)  2017 Nova Solution Systems S.A. de C.V.
* Mexico D.F.
* Todos los derechos reservados.
*
* @author René Hernández Ramírez
* @author Reynaldo Ivan Martinez Lopez
* @author Angel Martínez León
*
* ESTE SOFTWARE ES INFORMACIÓN CONFIDENCIAL. PROPIEDAD DE NOVA SOLUTION SYSTEMS.
* ESTA INFORMACIÓN NO DEBE SER DIVULGADA Y PUEDE SOLAMENTE SER UTILIZADA DE ACUERDO CON LOS TÉRMINOS DETERMINADOS POR LA EMPRESA SÍ MISMA.
*/

@RestController
@RequestMapping(value = "/")
public class AppControlador {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppControlador.class);

	@Autowired
	private MongoService mongo;
	
 


	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping(value = "/operacionesBasicas", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> operacionesBasicas(RequestEntity<Object> request) {

		Map<String, Object> sendRequestBody = new HashMap<String, Object>();

		sendRequestBody = (Map<String, Object>) request.getBody();

		Integer operacion = (Integer) sendRequestBody.get("operacion");

		switch (operacion) {
		case 1:// Altas

			System.out.println("Altas");
			Map<String, Object> mapaInsertar = new HashMap<String, Object>();
			Map<String, Object> documentoInsertar = new HashMap<String, Object>();
			Map<String, Object> mapaResultadoInserta = new HashMap<String, Object>();
			try {

				mapaInsertar = (Map<String, Object>) request.getBody();
				documentoInsertar.putAll((Map<String, Object>)mapaInsertar.get("documento"));

				String respuesta = mongo.inserta("preregistro", documentoInsertar);
 
				long responseStatus = 200;
				mapaResultadoInserta.put("responseStatus", responseStatus);
				mapaResultadoInserta.put("responseError", respuesta);
				return new ResponseEntity<Object>(mapaResultadoInserta, HttpStatus.OK);

			} catch (HttpMessageNotReadableException e) {

				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoInserta.put("responseStatus", 400);
				mapaResultadoInserta.put("responseError", e.getMessage());

				return new ResponseEntity<Object>(mapaResultadoInserta, HttpStatus.OK);

			} catch (Exception e) {
				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoInserta.put("responseStatus", 400);
				mapaResultadoInserta.put("responseError", e.getMessage());
				return new ResponseEntity<Object>(mapaResultadoInserta, HttpStatus.OK);
			}

		case 2:// Modificacion

			System.out.println("mod");
			Map<String, Object> indiceModificacion = new HashMap<String, Object>();
			Map<String, Object> mapaDatosConsulta = new HashMap<String, Object>();
			Map<String, Object> mapaDatosAModificar = new HashMap<String, Object>();
			Map<String, Object> mapaResultadoMod = new HashMap<String, Object>();

			Map<String, Object> mapaResultadoConsulta = new HashMap<String, Object>();
			Map<String, Object> mapaResultadoConsultaValor = new HashMap<String, Object>();
			List<String> datosAIgnorar = new ArrayList<>();

			Map<String, Object> mapaResultadofinal = new HashMap<String, Object>();

			WriteResult resultado = null;

			Operaciones operaciones = new Operaciones();
			try {
				mapaDatosConsulta = (Map<String, Object>) request.getBody();
				
				indiceModificacion.putAll((Map<String, Object>)  mapaDatosConsulta.get("indice"));

				mapaDatosAModificar.putAll((Map<String, Object>) mapaDatosConsulta.get("documento"));
 
				datosAIgnorar.add("_id");
				mapaResultadoConsulta = mongo.consulta("preregistro", indiceModificacion, datosAIgnorar);

				mapaResultadoConsultaValor
						.putAll((Map<? extends String, ? extends Object>) mapaResultadoConsulta.get("1"));

				if (mapaResultadoConsulta.size() >= 1 && mapaResultadoConsulta.size() <= 1) {
					mapaResultadofinal = operaciones.modifica(mapaResultadoConsultaValor, mapaDatosAModificar);

					resultado = mongo.modificacion("preregistro", indiceModificacion, mapaResultadofinal);
				}
				if (resultado.getLastError().ok()) {
					mapaResultadoMod.put("responseStatus", 200);
					mapaResultadoMod.put("responseError", "OK");
				} else {
					mapaResultadoMod.put("responseStatus", 400);
					mapaResultadoMod.put("responseError", "Error, Al Modificar");
				}
				return new ResponseEntity<Object>(mapaResultadoMod, HttpStatus.OK);
			} catch (HttpMessageNotReadableException e) {

				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoMod.put("responseStatus", 400);
				mapaResultadoMod.put("responseError", e.getMessage());
				return new ResponseEntity<Object>(mapaResultadoMod, HttpStatus.OK);

			} catch (Exception e) {
				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoMod.put("responseStatus", 400);
				mapaResultadoMod.put("responseError", e.getMessage());
				return new ResponseEntity<Object>(mapaResultadoMod, HttpStatus.OK);
			}

		case 3:// bajas
 
			System.out.println("Bajas");
			Map<String, Object> mapaResultadoBaja = new HashMap<String, Object>();
			Map<String, Object> indiceBaja = new HashMap<String, Object>();
			 
			try {
				mapaDatosConsulta = (Map<String, Object>) request.getBody();
				indiceBaja.putAll((Map<String, Object>) mapaDatosConsulta.get("indice"));
				
				WriteResult resultadoBaja = mongo.eliminar("preregistro", indiceBaja);

				if (resultadoBaja.getLastError().ok()) {
					mapaResultadoBaja.put("responseStatus", 200);
					mapaResultadoBaja.put("responseError", "OK");
				} else {
					mapaResultadoBaja.put("responseStatus", 400);
					mapaResultadoBaja.put("responseError", "Error, Al Eliminar");
				}
				return new ResponseEntity<Object>(mapaResultadoBaja, HttpStatus.OK);
			} catch (HttpMessageNotReadableException e) {

				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoBaja.put("responseStatus", 400);
				mapaResultadoBaja.put("responseError",e.getMessage());
				return new ResponseEntity<Object>(mapaResultadoBaja, HttpStatus.OK);

			} catch (Exception e) {
				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoBaja.put("responseStatus", 400);
				mapaResultadoBaja.put("responseError", e.getMessage());
				return new ResponseEntity<Object>(mapaResultadoBaja, HttpStatus.OK);
			}

		case 4:// consultaEspecifica

			System.out.println("COnsulta Especifica");
			Map<String, Object> mapaDatosConsultaEspecifica = new HashMap<String, Object>();
			Map<String, Object> mapaResultadoEspecifica = new HashMap<String, Object>();
			Map<String, Object> indiceEspecifica = new HashMap<String, Object>();

			List<String> datosAIgnorarEspecifica = new ArrayList<>();

			try {
				System.out.println("inicio servicio 4------------------------------------------------------");
				mapaDatosConsultaEspecifica = (Map<String, Object>) request.getBody();
				System.out.println("mapaDatosConsultaEspecifica ---------------------"+mapaDatosConsultaEspecifica);
				
				indiceEspecifica.putAll((Map<String, Object>) mapaDatosConsultaEspecifica.get("documento"));
				System.out.println("----------------------------------------------");
				System.out.println("indiceEspecifica---------------------------------------"+indiceEspecifica);
//				datosAIgnorarEspecifica.add("_id");
				
				mapaResultadoEspecifica = mongo.consulta("preregistro", indiceEspecifica,
						datosAIgnorarEspecifica);
				
				System.out.println(mapaResultadoEspecifica.get("_id"));
				
				return new ResponseEntity<Object>(mapaResultadoEspecifica.values(), HttpStatus.OK);
			} catch (HttpMessageNotReadableException e) {
				System.out.println("---------------------------------------catch 1");
				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoEspecifica.put("responseStatus", 400);
				mapaResultadoEspecifica.put("responseError", "Error, Al Consultar");
				return new ResponseEntity<Object>(mapaResultadoEspecifica, HttpStatus.OK);

			} catch (Exception e) {
				LOGGER.info("Error: " + e.getMessage());
				System.out.println("---------------------------------------catch 2");
				mapaResultadoEspecifica.put("responseStatus", 400);
				mapaResultadoEspecifica.put("responseError", "Error, Al Consultar");
				return new ResponseEntity<Object>(mapaResultadoEspecifica, HttpStatus.OK);
			}

		case 5:// consultaTotal

			System.out.println("Consulta total");
			Map<String, Object> mapaDatosConsultaTotal = new HashMap<String, Object>();
			Map<String, Object> mapaResultadoTotal = new HashMap<String, Object>();
			Map<String, Object> indiceTotal = new HashMap<String, Object>();

			List<String> datosAIgnorarTotal = new ArrayList<>();

			try {
				mapaDatosConsultaTotal = (Map<String, Object>) request.getBody();
				
				indiceTotal.putAll((Map<String, Object>) mapaDatosConsultaTotal.get("documento"));
				
				datosAIgnorarTotal.add("_id");
				mapaResultadoTotal = mongo.consulta("preregistro", indiceTotal,
						datosAIgnorarTotal);
				return new ResponseEntity<Object>(mapaResultadoTotal.values(), HttpStatus.OK);
			} catch (HttpMessageNotReadableException e) {

				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoTotal.put("responseStatus", 400);
				mapaResultadoTotal.put("responseError", "Error, Al Consultar");
				return new ResponseEntity<Object>(mapaResultadoTotal, HttpStatus.OK);

			} catch (Exception e) {
				LOGGER.info("Error: " + e.getMessage());
				mapaResultadoTotal.put("responseStatus", 400);
				mapaResultadoTotal.put("responseError", "Error, Al Consultar");
				return new ResponseEntity<Object>(mapaResultadoTotal, HttpStatus.OK);
			}

		default:
			System.out.println("Default");
			Map<String, Object> mapaDefault = new HashMap<String, Object>();
			mapaDefault.put("responseStatus", 400);
			mapaDefault.put("responseError", "Operacion no existe");
			return new ResponseEntity<Object>(mapaDefault, HttpStatus.OK);

		}
	}
	

	 
}
